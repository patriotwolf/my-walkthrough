# WizzardMap Component in Legacy

This readme file is explain what was happen during it's development

## Getting Started

Here's on how it work.
1) There's 6 component in *Wizzard* container.
* mapbox - use for pickup location, drop location, pickup location and drop location at summary scene
* marker - this is for start cordinate
* marker - this is for receiver cordinates

2) There's 2 method on how the coodinates update. It's either *By suggestion* or *Manual Input*

3) Update coordinates at latest coordinates recieved from endpoint so that the calculation are accurate logically.

### Instalation

There is file of Javascript and It's CSS for mapboxgl and also included Turf.js for Line curve draw


### Break down into front-end logic

1) MapBox will be trigger when template of **wizzard** is mounted.

2) After assigning map, there's a code to function on **load** that is to get coordinate of where are the user now.

3) In same time, Mapbox _navigationControl_ were added

4) For Pickup Coordinate:
* Marker are currently in user coordinate.
* Marker will be updated on suggestion displayed when use suggestion checkbox is checked. For manual, the coordinate obtain after submit.
* Marker will be displayed in this current condition when use suggestion. If manually, the marker will display when *wizzard* component start promt input for drop location

5) For Drop Coordinate:
* Marker Pickup will be displayed after obtained from **endpoint request**
* Same process like pickup but 
*

6) SyncCoord: Always tally-up the coordinate at syncCoord method.

7) You will find condition for map at summary of when the last two map condition should be displayed.