# MD-Auth


## Routing
This is the routing structure
  ``` 
  /page
    index.vue
    login.vue
    fogot.vue
    signup.vue
    callback.vue
    login.vue
    manage.vue
    /manage
      account.vue
      login_account.vue
      change_password.vue
      index.vue
      change.vue
      /change
        index.vue
        mail.vue
        phone.vue 
  ```
This is the best suitable and easy routing, unless you want to config the routing by yourself in _nuxt.config.js_

## Layout

### Default
  this layout is to put all page using this default layout in a div

### Single Card
  this layout is crafted for pages that use a card in a layout

### Error
  this layout is display when an error occured. You can see by *error* props's attribute to see what kind of error that been produced.

## Redirect at index page
As you see in page directory and it's sub directory has index.vue, You should do some redirecting in here and can make it as a template view to ease up you development.