# MD-Auth


## Routing
Please use route guide in _MD_AUTH_.
Dont make every page in a direcory.
just make a Vue file for indexing and layout control then create directory with same name.

## Layout

### Default
  this layout is have sidebar, navbar and container for nuxt display page.

### Error
  this layout is display when an error occured. You can see by *error* props's attribute to see what kind of error that been produced.


## Redirect at index page
As you see in page directory and it's sub directory has index.vue, You should do some redirecting in here and can make it as a template view to ease up you development.

## Recommendation
If could, please do unit testing. As I see, you will be having so many potential major and minor bugs after I've inspecting web legacy codes and experiencing how MatDespatch conducting front-end project. Don't worry, Rome wasn't built in a day in order to make it still standing until today. By the way, Unit testing on front-end is not the only way to avoid those bugs. It'll help, but not the only solution.

* [Architecting front-end](https://www.oreilly.com/library/view/frontend-architecture-for/9781491926772/ch01.html)
* [Basic things in becoming front-end](https://www.upwork.com/hiring/development/beginners-guide-to-front-end-development/)
* [Always validate research and design](https://raygun.com/blog/designing-for-developers/)
* [Planning a front-end](https://www.smashingmagazine.com/2018/02/comprehensive-website-planning-guide-part1/)

## GoodLuck
_Always make a plan, strategy and architect the front-end carefully so that it'll not inflicting pain resulting major change on back-end and make sure the back-end is in acceptable response format as using Ant-Design Vue Component is really wierd and not flexible enough on developement side. Unless you choosing client-side processing after server-side processing. My suggestion, do some playground before applying the component_
